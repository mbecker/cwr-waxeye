<?php


namespace parser\ast;


use JsonSerializable;

class AST implements JsonSerializable
{
    private string $type;
    private ASTs $children;
    private int $start;
    private int $end;

    /**
     * AST constructor.
     * @param string $type
     * @param ASTs $children
     * @param int $start
     * @param int $end
     */
    public function __construct(string $type, ASTs $children, int $start, int $end)
    {
        $convertedChildren = new ASTs();

        foreach ($children as $child) {
            $allChars = true;

            for ($i = 0; $i < $child->getChildren()->count(); $i++) {
                $grandChild = $child->getChildren()[$i];
                if (!$grandChild instanceof Char) {
                    $allChars = false;
                    break;
                }
            }

            if ($allChars && !$child instanceof Char) {
                $convertedChildren[] = new NTChar($child->getType(), $child->getChildrenAsString(), $child->getStart());
            } else {
                $convertedChildren[] = $child;
            }
        }

        $this->type = $type;
        $this->children = $convertedChildren;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return string
     */
    public
    function getType(): string
    {
        return $this->type;
    }

    /**
     * @return ASTs
     */
    public
    function getChildren(): ASTs
    {
        return $this->children;
    }

    public
    function getChildrenAsString(): string
    {
        $result = "";

        foreach ($this->children as $child) {
            $result .= Char::asCharAST($child)->getValue();
        }

        return $result;
    }

    /**
     * @return int
     */
    public
    function getStart(): int
    {
        return $this->start;
    }

    /**
     * @return int
     */
    public
    function getEnd(): int
    {
        return $this->end;
    }

    /**
     * @inheritDoc
     */
    public
    function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public
    function __toString()
    {
        return json_encode($this);
    }
}
