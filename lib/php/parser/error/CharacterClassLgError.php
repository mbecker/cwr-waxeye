<?php


namespace parser\error;


use JsonSerializable;
use parser\expression\CharClassLgExpression;
use RuntimeException;

class CharacterClassLgError implements MatchError, JsonSerializable
{
    private CharClassLgExpression $expression;

    /**
     * CharacterClassLgError constructor.
     * @param CharClassLgExpression $expression
     */
    public function __construct(CharClassLgExpression $expression)
    {
        $this->expression = $expression;
    }


    public function toGrammarString(): string
    {
        throw new RuntimeException("Class " . self::class . "->toGrammarString not implemented yet!");
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
