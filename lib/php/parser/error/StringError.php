<?php


namespace parser\error;


use JsonSerializable;
use parser\expression\StringExpression;
use RuntimeException;

class StringError implements MatchError, JsonSerializable
{
    private StringExpression $stringExpression;

    /**
     * StringError constructor.
     * @param StringExpression $stringExpression
     */
    public function __construct(StringExpression $stringExpression)
    {
        $this->stringExpression = $stringExpression;
    }


    public function toGrammarString(): string
    {
        throw new RuntimeException("Class " . self::class . "->toGrammarString not implemented yet!");
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }
}
