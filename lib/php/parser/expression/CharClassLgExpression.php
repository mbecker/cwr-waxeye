<?php


namespace parser\expression;


class CharClassLgExpression extends Expression
{
    private int $length;
    private CharClassExpression $charClassExpression;

    /**
     * CharClassLgExpression constructor.
     * @param int $length
     * @param CharClassExpression $charClassExpression
     */
    public function __construct(int $length, CharClassExpression $charClassExpression)
    {
        parent::__construct(ExpressionType::CHAR_CLASS_LG);

        $this->length = $length;
        $this->charClassExpression = $charClassExpression;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * @return CharClassExpression
     */
    public function getCharClassExpression(): CharClassExpression
    {
        return $this->charClassExpression;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return json_encode($this);
    }

    public static function asCharClassLgExpression($expression): CharClassLgExpression
    {
        return $expression;
    }
}
