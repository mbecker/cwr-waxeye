### Usage
mvn package erzeugt das Verzeichnis build, in welchem sich die Parser für die jeweiligen Sprachen sowie eine kleine Beispielanwendung befinden.

#### Ausführung Javascript
Im Verzeichnis build/javascript befinden sich alle Dateien, die notwendig sind, um den Parser auf einem Server auszuführen. Die Datei Application.html enthält ein Beispiel zur Nutzung des Parser. Dieser nutzt die Bibliothek require.js. Beim Ausführen auf einem Server muss eventuell der Pfad angepasst werden.

Zur Ausführung mit node.js gibt es die Datei Application.js. Diese wird mittels `node Application.js cwrFile` aufgerufen und verarbeitet eine CWR-Datei auf der Kommandozeile. Das Ergebnis wird in die Datei cwrFile.json gespeichert.

#### Ausführung PHP
Im Verzeichnis build/php befinden sich alle Dateien, die notwendig sind, um den PHP-Parser auszuführen. Dieser wird auf der Kommandozeile mittel `php CommandLineApplication.php cwrFile` gestartet und schreibt das Ergebnis der Verarbeitung in die Datei cwrFile.json.

#### Ausführung Java
Im Verzeichnus build/java befinden sich alle Dateien, die notwendig sind, um den Java-Parser auszuführen. Dieser wird mittels `java -classpath parser-0.1.jar; de.unileipzig.urz.soclear.cwr.CommandLineApplication cwrFile` aufgerufen. Das Ergebnis der Verarbeitung befindet sich dann in der Datei cwrFile.result.

für die Zukunft: https://stackoverflow.com/questions/4955635/how-to-add-local-jar-files-to-a-maven-project