package de.unileipzig.urz.soclear.test.cwr;

import de.unileipzig.urz.soclear.cwr.Parser;
import de.unileipzig.urz.soclear.cwr.Type;
import org.waxeye.parser.ParseResult;

public class ParserTest {
    public static void main(String[] args) {
        final Parser parser = new Parser();
        final ParseResult<Type> result = parser.parse("HDR123");
        System.out.println(result);
    }
}
