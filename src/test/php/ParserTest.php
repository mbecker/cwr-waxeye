<?php

spl_autoload_register(function ($class_name) {
    include "C:/Users/micha/IdeaProjects/waxeye-soclear/src/php/" . $class_name . ".php";
});

include "../../../target/generated-sources/waxeye-php/GenParser.php";
$inputFile = "result.txt";
$outputFile = "../../../target/result.json";

$parser = new GenParser();
try {
    $input = file_get_contents($inputFile);
    $result = $parser->parse($input);
    file_put_contents($outputFile, $result);
} catch (ParseException $exception) {
    printf("%s: parse error at position %s (line %s, col %s), expected: %s, read: %s\n", $inputFile, $exception->getParseError()->getPosition(), $exception->getParseError()->getLine(), $exception->getParseError()->getColumn(), $exception->getParseError()->getChars(), $input[$exception->getParseError()->getPosition()]);
}