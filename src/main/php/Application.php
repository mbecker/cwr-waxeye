<?php

use parser\error\ParseException;

spl_autoload_register(function ($class_name) {
    include "C:/Users/micha/IdeaProjects/waxeye-soclear/src/php/" . $class_name . ".php";
});

include "../../../target/generated-sources/waxeye-php/GenParser.php";

$files = array("CW187591KTF_000.V21", "CW190004KTF_000.V21", "CW190005KTF_000.V21", "CW200021STA_035.V21", "CW170001035_jgr.V21", "CW190004000_KTF.v21", "CW190005000_KTF.v21", "CW200016035_STA.V21");
$directory = "../resources/cwr-files/";
$outputDirectory = "../../../target/php-cwr-parsing/";
$parser = new GenParser();

if (!is_dir($outputDirectory)) {
    mkdir($outputDirectory);
}

$timeStart = microtime(true);
foreach ($files as $file) {
    $timeStartFile = microtime(true);
    printf("parsing %s ...", $file);

    try {
        $input = file_get_contents($directory . $file);
        $result = $parser->parse($input);
        file_put_contents($outputDirectory . $file . ".json", $result);
    } catch (ParseException $exception) {
        printf("%s: parse error at position %s (line %s, col %s), expected: %s, read: %s\n", $file, $exception->getParseError()->getPosition(), $exception->getParseError()->getLine(), $exception->getParseError()->getColumn(), $exception->getParseError()->getChars(), $input[$exception->getParseError()->getPosition()]);
    }

    $executionTimeFile = microtime(true) - $timeStartFile;
    printf(" ... done, took %s seconds\n", $executionTimeFile);
}
$timeEnd = microtime(true);
$executionTime = ($timeEnd - $timeStart);

printf("Execution Time: %s seconds\n", $executionTime);