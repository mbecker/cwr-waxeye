<?php

use parser\error\ParseException;

spl_autoload_register(function ($class_name) {
    include $class_name . ".php";
});

include "GenParser.php";

if (2 !== count($argv)) {
    throw new RuntimeException("Usage: php CommandLineApplication.php cwrFilename");
}

$cwrFile = $argv[1];
if (!is_file($cwrFile)) {
    throw new RuntimeException("File " . $cwrFile . " not found!");
}

$parser = new GenParser();
$input = file_get_contents($cwrFile);


try {
    $result = $parser->parse($input);
    file_put_contents(basename($cwrFile) . ".json", $result);
} catch (ParseException $exception) {
    printf("%s: parse error at position %s (line %s, col %s), expected: %s, read: %s\n", $cwrFile, $exception->getParseError()->getPosition(), $exception->getParseError()->getLine(), $exception->getParseError()->getColumn(), $exception->getParseError()->getChars(), $input[$exception->getParseError()->getPosition()]);
}