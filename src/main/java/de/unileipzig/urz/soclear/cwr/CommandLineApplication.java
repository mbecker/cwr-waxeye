package de.unileipzig.urz.soclear.cwr;

import org.waxeye.parser.ParseResult;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class CommandLineApplication {
    public static void main(String[] args) throws Exception {
        Path file = Path.of(args[0]);
        Path resultFile = Path.of(file.getFileName() + ".result");

        if (!Files.exists(file)) {
            throw new RuntimeException("File " + file + " does not exist. Usage java CommandLineApplication cwrFile");
        }

        List<String> lines = Files.readAllLines(file);
        String fileContent = String.join("\n", lines);
        final Parser parser = new Parser();
        final ParseResult<Type> result = parser.parse(fileContent);

        if (null != result.getError()) {
            throw new RuntimeException(result.getError().toString());
        } else {
            Files.writeString(resultFile, result.toString(), Charset.defaultCharset());
        }
    }
}
