package de.unileipzig.urz.soclear.cwr;

import org.waxeye.parser.ParseResult;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Application {
    public static void main(String[] args) throws Exception {
        String inputDirectory = "/cwr-files";
        String outputDirectory = "target/java-cwr-parsing";
        String[] files = {"CW187591KTF_000.V21", "CW190004KTF_000.V21", "CW190005KTF_000.V21", "CW200021STA_035.V21", "CW170001035_jgr.V21", "CW190004000_KTF.v21", "CW190005000_KTF.v21", "CW200016035_STA.V21"};
        //String[] files = {"CW200016035_STA.V21"};

        if (!Files.exists(Path.of(outputDirectory))) {
            Files.createDirectory(Path.of(outputDirectory));
        }

        for (String file : files) {
            System.out.print("parsing " + file + " ... ");

            try {
                String result = parse(getFileContents(inputDirectory + "/" + file));
                new FileWriter(outputDirectory + "/" + file + ".result").write(result);
            } catch (Exception e) {
                System.out.print(e.getMessage());
            }

            System.out.println("| parsing done");
        }
    }

    private static String parse(String content) {
        final Parser parser = new Parser();
        final ParseResult<Type> result = parser.parse(content);

        if (null != result.getError()) {
            throw new RuntimeException(result.getError().toString());
        } else {
            return result.toString();
        }
    }

    private static String getFileContents(String filename) throws Exception {
        List<String> lines = Files.readAllLines(Paths.get(Application.class.getResource(filename).toURI()));
        return String.join("\n", lines);
    }
}
