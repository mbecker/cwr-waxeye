const file = process.argv[2];
if (file === undefined) {
    console.log("Usage: node Application.js cwrFile!")
    return -1;
}

const fs = require('fs');
if (!fs.existsSync(file)) {
    console.log("File " + file + " was not found!");
    return -1;
}

const parser = require('./parser');
const waxeye = require("./waxeye/waxeye");

const p = new parser.Parser();
const fileContent = fs.readFileSync(file).toString();
const result = p.parse(fileContent);

if (result instanceof waxeye.ParseError) {
    console.log("error parsing " + file + ": " + JSON.stringify(result));
} else {
    const path = require('path');
    fs.writeFileSync(path.basename(file) + ".json", JSON.stringify(result));
}