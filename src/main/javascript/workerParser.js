self.importScripts("./require.js");
const parserModule = Tarp.require({main: "./parser.js", sync: true});
const parser = new parserModule.Parser();

self.onmessage = function (message) {
    parse(message.data.content);
}

function parse(content) {
    const result = parser.parse(content);

    if (undefined === result.type) {
        postMessage({
            type: "error",
            error: result.toString(),
            pos: result.pos,
            line: result.line,
            col: result.col,
            nt: result.nt,
            chars: result.chars
        });
    } else {
        postMessage({type: "parseResult", result: result});
    }
}

