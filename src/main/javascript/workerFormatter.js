function formatContent(parseResult, whitespaces) {
    postMessage({type: "info", content: "formatting " + parseResult.end + " characters... "})

    parseResult.children.forEach(function (child) {
        parseContent(child, whitespaces);
    });

    postMessage({type: "info", content: "finished"});
}

function parseContent(parserContent, whitespaces) {
    switch (parserContent.type) {
        case "TransmissionBegin": {
            postMessage({type: "transmissionHeader", content: formatChildrenWithReturn(parserContent, whitespaces)});
            break;
        }
        case "GroupBegin": {
            postMessage({type: "groupHeader", content: formatChildrenWithReturn(parserContent, whitespaces)});
            break;
        }
        case "Transaction" : {
            postMessage({type: "transaction", content: formatChildrenWithReturn(parserContent, whitespaces)});
            break;
        }
        case "GroupEnd": {
            postMessage({type: "groupTrailer", content: formatChildrenWithReturn(parserContent, whitespaces)});
            break;
        }
        case "TransmissionEnd": {
            postMessage({type: "transmissionTrailer", content: formatChildrenWithReturn(parserContent, whitespaces)});
            break;
        }
        default: {
            if (undefined !== parserContent.children) {
                parserContent.children.forEach(child => parseContent(child, whitespaces));
            } else if (undefined !== parserContent.value) {
                postMessage({type: parserContent.type, content: parserContent.value});
            }
            break;
        }
    }
}

function formatChildrenWithReturn(parent, whitespaces) {
    if (null != parent.children) {
        let childContent = "";
        parent.children.forEach(function (child) {
            childContent += formatChildrenWithReturn(child, whitespaces);
        })

        return childContent;
    } else if (null != parent.type && parent.type === "LINEBREAK") {
        return "<br>";
    } else if (null != parent.value) {
        const value = parent.value;
        const type = parent.type;

        if (value === "" && whitespaces === "hide") {
            return "";
        } else {
            let nodeContent = "<span title='" + type + "' class='" + type + "'>"
            if (whitespaces === "trim") {
                nodeContent += value.trim();
            } else if (whitespaces === "html") {
                nodeContent += value.replace(/ /g, "&nbsp;");
            } else {
                nodeContent += value;
            }
            nodeContent += "</span>";

            return nodeContent;
        }
    } else {
        return "<span title='" + parent + "' class='" + parent + "'>" + parent + "</span>";
    }
}

self.onmessage = function (message) {
    const parseResult = message.data.parseResult;
    const whitespaces = "html";

    // whitespaces possible values:
    // trim: trim whitespaces in all elements
    // none: trim + hide empty elements
    // html: display all whitespaces using &nbsp;
    // all other values: don't do anything

    formatContent(parseResult, whitespaces);
}